module.exports = function (grunt) {
    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        uglify: {
            build: {
                files: [{
                    expand: true,
                    src: '*.js',
                    dest: 'build/js',
                    cwd: 'src/js/',
                    ext: '.min.js'
                }],
                options: {
                    mangle: false,
                    banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - ' +
                    '<%= grunt.template.today("yyyy-mm-dd") %> */'
                }
            }

        },
        sass: {
            dist: {
                options: {
                    style: 'compact'
                },
                files: {
                    'build/css/styles.css': 'src/styles/styles.scss'

                }
            }
        },
        cssmin: {
            target: {
                files: [{
                    expand: true,
                    cwd: 'build/css/',
                    src: ['*.css', '!*.min.css'],
                    dest: 'build/css/',
                    ext: '.min.css'
                }]
            }
        },
        copy: {
            index: {
                src: 'index.html',
                dest: 'build/index.html',
                options: {
                    process: function (content, srcpath) {
                        return content.replace(/build\//g,'').replace(/src\//g,'');
                    }
                }
            },
            build: {
                files: [
                    // images
                    {expand: true, cwd: 'src/images/', src: ['**'], dest: 'build/images/'},
                    // sprites
                    {expand: true, cwd: 'src/sprites/', src: ['**'], dest: 'build/sprites/'},
                    // fonts
                    {expand: true, cwd: 'src/fonts/', src: ['**'], dest: 'build/fonts/'}
                ]
            }
        },
        serve: {
            options: {
                path: '/',
                port: 9000
            }
        },
        watch: {
            scripts: {
                files: ['src/js/**/*.js'],
                tasks: ['uglify']

            },
            sass: {
                files: ['src/styles/**/*.scss'],
                tasks: ['sass']
            },
            cssmin: {
                files: ['src/styles/**/*.scss'],
                tasks: ['cssmin']
            },
            copy: {
                files: ['index.html', 'src/images/*', 'src/sprites/*','src/fonts/*'],
                tasks: ['copy']
            }
        }
    });

    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-serve');
    grunt.registerTask('default', ['sass', 'uglify', 'cssmin', 'copy']);

};