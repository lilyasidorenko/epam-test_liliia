var $overlay = $('.js-popup-overlay'),
    $openModal = $('.js-popup-init'),
    $close = $('.js-popup-close, .js-popup-overlay'),
    $modal = $('.js-popup'),
    $body = $('body');

$openModal.click( function(event){
    event.preventDefault();
    var div = $(this).attr('href');
    $body.css('overflow', 'hidden');
    $overlay.fadeIn(400,
        function(){
            $(div)
                .css('display', 'block')
                .animate({opacity: 1}, 200);
        });
});

$close.click( function(){
    $body.removeAttr('style');
    $modal
        .animate({opacity: 0}, 200,
        function(){
            $(this).css('display', 'none');
            $overlay.fadeOut(400);
        }
    );
});