$('.js-trigger').on('click', function () {
    $(this).next().toggle()
});

$(window).on('load resize', function () {
    if ($(this).outerWidth() >= 620) {
        $('.js-trigger').next().removeAttr('style')
    }
});